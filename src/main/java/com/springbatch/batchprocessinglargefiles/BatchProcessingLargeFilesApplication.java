package com.springbatch.batchprocessinglargefiles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchProcessingLargeFilesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BatchProcessingLargeFilesApplication.class, args);
	}

}

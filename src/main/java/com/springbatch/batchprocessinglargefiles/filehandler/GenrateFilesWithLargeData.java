package com.springbatch.batchprocessinglargefiles.filehandler;

import java.io.FileWriter;

public class GenrateFilesWithLargeData {

	public static String dummyString = "     11-191675  01  01/14/2013  2368183100   OUNHQEX XUFQONY            S1   No            Yes                             000    Yes";
	public static int totalCount = 12_345_6;
	public static void main(String[] args) {
		long mainTime = System.currentTimeMillis();
		try {

			Thread t1 = new Thread(new Runnable() {
				public void run() {
					System.out.println("Input File Started.");
					long time = System.currentTimeMillis();
					try {
						FileWriter myWriter = new FileWriter("1A-inputFile.txt");
						for(long i = totalCount ; i > 100;i--) {
							myWriter.write(Commons.getKey(i+"", 9) + dummyString);
							myWriter.write(System.lineSeparator());
						}
						myWriter.close();
						System.out.println("Input File - " + (System.currentTimeMillis() - time));
					} catch (Exception e) {
						e.printStackTrace();
					}finally {
						synchronized (this) {
							System.out.println("NotifyAll Threads....");
							this.notifyAll();
						}
					}
				}

			}, "InputFile");

			Thread t2 = new Thread(new Runnable() {
				public void run() {
					try {
						long time = System.currentTimeMillis();
						FileWriter myWriter = new FileWriter("1A-searchFile.txt");
						for(long i = 10000 ; i < totalCount;i++) {
							if(i > 1000000 && i % 2 == 0) {
								myWriter.write(Commons.getKey(i+"", 9) + dummyString );
								myWriter.write(System.lineSeparator());
							}
						}
						myWriter.close();
						System.out.println("Search File - " + (System.currentTimeMillis() - time));
					} catch (Exception e) {
						e.printStackTrace();
					}finally {
						synchronized (this) {
							this.notifyAll();
						}
					}
				}
			});
			
			t1.start();
			t2.start();
			synchronized (t1) {
				System.out.println("Waiting for thread to complete....");
				t1.wait();
			}
			
			//t1.join();
			//t2.join();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Time Main - " + (System.currentTimeMillis() - mainTime));
	}

}

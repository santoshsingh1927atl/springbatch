package com.springbatch.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

@Configuration
@EnableBatchProcessing
public class BatchLearning {

		@Autowired
	    public JobBuilderFactory jobBuilderFactory;

	    @Autowired
	    public StepBuilderFactory stepBuilderFactory;
	    
	    @Value("${file.input}")
	    private String fileInput;
	    
	    @Value("${file.output}")
	    private String output;
	    
	    @Autowired
	    JobCompletionNotificationListener jobCompletionNotificationListener;
	    
	    @Bean
	    public FlatFileItemReader reader() {
	        return new FlatFileItemReaderBuilder().name("coffeeItemReader")
	          .resource(new ClassPathResource(fileInput))
	          .delimited()
	          .names(new String[] { "brand", "origin", "characteristics" })
	          .fieldSetMapper(new BeanWrapperFieldSetMapper() {{
	              setTargetType(Coffee.class);
	          }})
	          .build();
	    }
	    
	    
	    @Bean
	    public Job cofeeJob() {
	        return jobBuilderFactory.get("cofreejob")
	          .incrementer(new RunIdIncrementer())
	          .listener(jobCompletionNotificationListener)
	          .flow(step1())
	          .end()
	          .build();
	    }

	    @Bean
	    public Step step1() {
	        return stepBuilderFactory.get("step1")
	          .<Coffee, Coffee> chunk(10)
	          .reader(reader())
	          .processor(processor())
	          .writer(writer())
	          .build();
	    }
	    
	    @Bean
	    public FlatFileItemWriter<Coffee> writer() 
	    {
	        //Create writer instance
	        FlatFileItemWriter<Coffee> writer = new FlatFileItemWriter<>();
	        Resource outputResource = new FileSystemResource(output);
	        //Set output file location
	        writer.setResource(outputResource);
	         
	        //All job repetitions should "append" to same output file
	        writer.setAppendAllowed(true);
	         
	        //Name field values sequence based on object properties 
	        writer.setLineAggregator(new DelimitedLineAggregator<Coffee>() {
	            {
	                setDelimiter(",");
	                setFieldExtractor(new BeanWrapperFieldExtractor<Coffee>() {
	                    {
	                        setNames(new String[] { "brand", "origin", "characteristics" });
	                    }
	                });
	            }
	        });
	        return writer;
	    }

	    @Bean
	    public CoffeeItemProcessor processor() {
	        return new CoffeeItemProcessor();
	    }
}
